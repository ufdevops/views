def jobNames = [
	'job1-ttt',
	'job2-ttt',
	'job3-ttt',
	'job4-ttt'
]
def branches = [
	'master-ttt',
	'v1-ttt',
	'v2-ttt',
	'v3-ttt'
]
def buildTypes = [
	'dev-ttt',
	'prod-ttt',
	'tst-ttt',
	'uat-ttt'
]

nestedView('main-view-ttt') {
	branches.each { branchName ->
		views {
			nestedView('jobs-for-' + branchName) {
				buildTypes.each { buildType ->
					views {
						nestedView(branchName + '-' + buildType + '-jobs') {
							views {
								listView(
								jobs {
									jobNames.each { jobName ->
										name(jobName)
									}
								}
								columns {
									status()
									weather()
									name()
									lastSuccess()
									lastFailure()
									lastDuration()
									buildButton()
								}
							}
						}
					}
				}
			}
    }
	}
}
