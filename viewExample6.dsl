def devTeams = [
	'DWA_GGG',
	'Hybris_GGG'
]
def buildTypes = [
	'Dev_GGG',
	'Prod_GGG',
	'TST_GGG',
	'UAT_GGG'
]

nestedView('DevTeams_GGG') {
	devTeams.each { teamName ->
		views {
			nestedView(teamName) {
				buildTypes.each { buildType ->
					views {
						nestedView(teamName + ' ' + buildType) {
							views {
								listView(teamName + ' ' + buildType) {
									jobs {
										regex(/^Dev_UComm.*/)
									}
									columns{
										status()
										weather()
										name()
										lastSuccess()
										lastFailure()
										lastDuration()
										buildButton()
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
